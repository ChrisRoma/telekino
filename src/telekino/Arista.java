package telekino;


public class Arista {
	  private int A;
	  private int B;

	  
	  public Arista(int a, int b) {
		 if (a<0 || b<0)
			 throw new IllegalArgumentException("No se permiten valores menores a 0");
		 
		 else {
			 A=a;
			 B=b;
		 }
	  }
	  
	  public int getA() {
		 return A;
	  }
	  public int getB() {
		 return B;
	  }

	 
	  public boolean SonIguales(Arista C) {
		 if(this.A==C.getA()&&this.B==C.getB()) {
			 return true;
		 }else {
			 return false;
		 }
	  }
	 
	 
	  @Override
	  public String toString() {
		return A + "," + B;
	  }

	  @Override
		public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		if (!SonIguales(other))
			return false;
		return true;
		}
	}
